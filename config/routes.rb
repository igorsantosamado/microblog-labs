Rails.application.routes.draw do
  root 'static#home'
  get 'about' => 'static#about'
  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  get '/search/:name' => 'users#search', as: 'search'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
end

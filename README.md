# Microblog Labs

Fornece a possibilidade de compartilhar informações de forma rápida e receber 
atualizações em tempo real.

Você pode seguir outras pessoas e receber atualizações em seu feed assim como ter 
seguidores para suas atualizações.

Projeto exemplo feito para: [HE:labs](http://helabs.com/pt/)
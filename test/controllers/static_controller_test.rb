require 'test_helper'

class StaticControllerTest < ActionController::TestCase
  def setup
    @title = "Microblog Labs"
  end
  
  test "needs to get about page and correct verify title" do
    get :about
    assert_response :success
    assert_select "title", I18n.t("static.about.title") + " - #{@title}"
  end
  
  test "needs to get home page and verify correct title" do
    get :home
    assert_response :success
    assert_select "title", "Home - #{@title}"
  end

end

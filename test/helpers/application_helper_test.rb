require 'test_helper'

class AplicationHelperTest < ActionView::TestCase
  test "Complete title helper" do
    assert_equal complete_title, 'Microblog Labs'
    assert_equal complete_title('Home'), "Home - Microblog Labs"
    assert_equal complete_title('About'), "About - Microblog Labs"
    assert_equal complete_title('Sign Up'), "Sign Up - Microblog Labs"
  end
  
end
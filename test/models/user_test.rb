require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "Igor", email: "igor@email.com", 
      password: "psswrd", password_confirmation: "psswrd")
  end
  
  test 'should be valid' do
    assert @user.valid?
  end
  
  test 'name should be present' do
    @user.name = " "
    assert_not @user.valid?
  end
  
  test 'email should be present' do
    @user.email = " "
    assert_not @user.valid?
  end
  
  test 'name should not be long' do
    @user.name = 'i' * 51
    assert_not @user.valid?
  end
  
  test 'email shoul not be long' do
    @user.email = 'i' * 246 + '@email.com'
    assert_not @user.valid?
  end
  
  test 'should accept valid email address' do
    valid_emails = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org 
      first.last@foo.jp alice+bob@baz.cn]
    
    valid_emails.each do |valid_email|
      @user.email = valid_email
      assert @user.valid?, "#{valid_email.inspect} should be valid"
      
    end
  end
  
  test 'should accept invalid email address' do
    invalid_emails = %w[user@example,com user_at_foo.org user.name@example. 
      foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    
    invalid_emails.each do |invalid_email|
      @user.email = invalid_email
      assert_not @user.valid?, "#{invalid_email.inspect} should be invalid"
    end
  end
  
  test 'email should be unique' do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test 'email should be saved as lowercase' do
    mixed_email = "IgOr@EmAiL.CoM"
    @user.email = mixed_email
    @user.save
    assert_equal mixed_email.downcase, @user.reload.email 
  end
  
  test "password should be present" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "i" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end
  
  test 'associated microposts should be destroyed' do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
  
  test "feed should have the right posts" do
    igor = users(:igor)
    fulano  = users(:fulano)
    zeltrano    = users(:zeltrano)
    # Posts from followed user
    igor.microposts.each do |post_following|
      assert fulano.feed.include?(post_following)
    end
    # Posts from self
    igor.microposts.each do |post_self|
      assert igor.feed.include?(post_self)
    end
    # Posts from unfollowed user
    zeltrano.microposts.each do |post_unfollowed|
      assert_not igor.feed.include?(post_unfollowed)
    end
  end
end

require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase
  
  def setup
    @relationship = Relationship.new(follower_id: 1, followed_id: 2)
  end

  test "should be valid" do
    assert @relationship.valid?
  end

  test "should require a follower_id" do
    @relationship.follower_id = nil
    assert_not @relationship.valid?
  end

  test "should require a followed_id" do
    @relationship.followed_id = nil
    assert_not @relationship.valid?
  end
  
  test "should follow and unfollow a user" do
    igor = users(:igor)
    fulano  = users(:fulano)
    igor.unfollow(fulano)
    assert_not igor.following?(fulano)
    igor.follow(fulano)
    assert igor.following?(fulano)
    assert fulano.followers.include?(igor)
    igor.unfollow(fulano)
    assert_not igor.following?(fulano)
  end
end

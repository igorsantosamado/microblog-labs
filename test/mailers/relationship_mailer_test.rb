require 'test_helper'
include ActionView::Helpers::UrlHelper

class RelationshipMailerTest < ActionMailer::TestCase
  def setup
    @user = users(:igor)
  end
  
  test "following_notification" do
    other_user = @user.following.where(name: "Fulano").first
    mail = RelationshipMailer.following_notification(@user, other_user)
    assert_equal I18n.t("relationship_mailer.following_notification.subject"), 
        mail.subject
    assert_equal [other_user.email], mail.to
    assert_equal ["noreply@microbloglabs.herokuapp.com"], mail.from
    assert_match other_user.name, mail.body.encoded
    assert_match /http:\/\/localhost:3000\/users\/#{@user.id}/, mail.body.encoded
  end

end

# Preview all emails at http://localhost:3000/rails/mailers/relationship_mailer
class RelationshipMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/relationship_mailer/following_notification
  def following_notification
    user = User.first
    other_user = user.following.find(3)
    RelationshipMailer.following_notification(user, other_user)
  end

end

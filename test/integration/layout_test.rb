require 'test_helper'

class LayoutTest < ActionDispatch::IntegrationTest  
  test "layout page links" do
    get root_path
    assert_template 'static/home'
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", signup_path
    get signup_path
    assert_select "title", complete_title("Sign Up")
  end
end

require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:igor)
  end

  test "micropost interface" do
    login_as(@user)
    get root_path
    assert_select 'div.pagination'
    assert_select 'input[type=file]'
    
    assert_no_difference 'Micropost.count' do
      post microposts_path, micropost: { content: "" }
    end
    assert_select 'div#error_explanation'
   
    content = "This micropost really ties the room together"
    picture = fixture_file_upload('test/fixtures/glider_3.jpg', 'image/jpg')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, micropost: { content: content, picture: picture }
    end
    micropost = assigns(:micropost)
    assert micropost.picture?
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    
    assert_select 'a', text: I18n.t("microposts.delete_link")
    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    
    get user_path(users(:fulano))
    assert_select 'a', text: 'delete', count: 0
  end
  
  test "micropost sidebar count" do
    login_as(@user)
    get root_path
    assert_match "#{@user.microposts.count} microposts", response.body
    # User with zero microposts
    other_user = users(:ciclano)
    login_as(other_user, password: "pwd223")
    get root_path
    assert_match "0 microposts", response.body
    other_user.microposts.create!(content: "A micropost")
    get root_path
    assert_match "1 micropost", response.body
  end
end

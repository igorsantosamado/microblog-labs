require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:beltrano)
  end
  
  test "show not unactivated user" do
    login_as(@user)
    get user_path(@user)
    assert_redirected_to root_url
  end
end

class Micropost < ActiveRecord::Base
  # Relationship
  belongs_to :user
  
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  
 # Validation 
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validate  :picture_size
  
  private
  
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, t("micropost.picture"))
    end
  end

end

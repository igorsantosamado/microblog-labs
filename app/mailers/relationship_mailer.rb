class RelationshipMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.relationship_mailer.following_notification.subject
  #
  def following_notification(user, other_user)
    @user = user 
    @other_user = other_user
    mail to: other_user.email, subject: default_i18n_subject
  end
end

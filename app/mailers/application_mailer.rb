class ApplicationMailer < ActionMailer::Base
  default from: "noreply@microbloglabs.herokuapp.com"
  layout 'mailer'
end

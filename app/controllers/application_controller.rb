class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  before_action :set_locale
  
  private
  
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = t(".login_msg")
      redirect_to login_url
    end
  end
  
  def set_locale
    locale = params[:locale] || cookies[:locale]
    
    if locale.present?
      I18n.locale = locale
      cookies[:locale] = { value: locale, expires: 30.days.from_now }
    end
  end
end

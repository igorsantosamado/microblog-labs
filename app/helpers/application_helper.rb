module ApplicationHelper
  
  def complete_title(title = "")
    principal = "Microblog Labs"
    if title.empty?
      principal
    else
      title + " - " + principal
    end
  end
end
